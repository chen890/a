import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreCollectionGroup } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {

  userCollection: AngularFirestoreCollection= this.db.collection('Users');

  constructor(private http: HttpClient,
              public db:AngularFirestore,
              public router:Router) 
  {
    
  }

  private url = "https://gm86wc72n9.execute-api.us-east-1.amazonaws.com/beta";
  public categories:object = {0: 'Business', 1: 'Entertainment', 2: 'Politics', 3: 'Sport', 4: 'Tech'}
  public doc:string; 
  
  classify():Observable<any>
  {
    console.log("this is the doc: ",this.doc);
    let json = {
                "articles": [ {"text": this.doc}, ]
              }
    let body  = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
                                                    map(
                                                        res => 
                                                        {
                                                          let final = res.body.replace('[','');
                                                          final = final.replace(']','');
                                                          console.log("this is final: ",final);
                                                          return final;      
                                                        }
                                                      )
                                                  );      
  }

  addClassification(userID:string, doc:string,category:string)
  {
    console.log('userID in addClassification(): ', userID)
    console.log('doc in addClassification(): ', doc)
    console.log('category in addClassification(): ', category)
    //this.db.collection('Books').add(book);
    if(category=="selected")
    {
      category = "selected"
    }
    else
    {
      const article={ userID:userID, doc:doc, category:category}
    }
    const article={ userID:userID, doc:doc, category:category}
    this.userCollection.doc(userID).collection('Articles').add(article);
    console.log("article: " ,article)
    this.router.navigate(['/classtified_collection'])    
  }
  
}
