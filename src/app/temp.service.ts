import { map, catchError } from 'rxjs/operators';
import { WeatherRaw } from './interfaces/weather-raw';
import { Weather } from './interfaces/weather';
import { Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class TempService {
  private URL= "http://api.openweathermap.org/data/2.5/weather?q=";
  private KEY= "be6318478ef2af390260d082d0a32cbc";
  private IMP= "&units=metric";

  searchWeatherData(cityName: string):Observable<Weather>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`)
    .pipe(map(data => this.transformWeatherData(data)),
    catchError(this.handelError)            //catchError מילה שמורה בעולם אובסרבל ומוציאה הודעת שגיאה 
    );     // דולר זה סטרינג ליטרלס
  } 

  private handelError(res: HttpErrorResponse)         //res מילה שהומצאה
  {
    console.log('this is the res error: '+res.error)
    return throwError(res.error);
  }


  private transformWeatherData(data: WeatherRaw): Weather
  {
    return {
      name:data.name,
      country:data.sys.country,
      image: `http://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
      description:data.weather[0].description,
      temperature:data.main.temp,
      lat:data.coord.lat,
      lon:data.coord.lon
    }
  }

  constructor(private http:HttpClient) 
  {

  }

  


}
