import { ArticlesService } from './../articles.service';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-article-edit',
  templateUrl: './article-edit.component.html',
  styleUrls: ['./article-edit.component.css']
})

export class ArticleEditComponent implements OnInit 
{
  book: any;
  form:any;
  category: string;
  doc:string;
  key_id;
  isEdit:boolean = false;
  buttonText:string="Add New Book";
  userID:string;
  articles$ : Observable<any>; 

  constructor(private route:ActivatedRoute,
              private router:Router,
              public articleService: ArticlesService,
              public authService: AuthService
              )
              { }

  ngOnInit() 
  {
      this.key_id=this.route.snapshot.params.key_id;
      this.authService.getUser().subscribe(        // מייבא את מספר המשתמש
                                            user=>
                                            {
                                              this.userID = user.uid
                                              console.log('userID: ', this.userID);
                                            });
      console.log("the article id: "+this.key_id);
      console.log("this isEdit: ",this.isEdit)
      // if(this.key_id !=null)
      // {
      //   console.log("key_id not null")
      //   this.isEdit=true;
      //   console.log("this isEdit: ",this.isEdit)
      //   this.buttonText="Update article";
      //   console.log("Before this.articleService.getOneArticle(this.userID,this.key_id): userID: ", this.userID)
      //   this.articleService.getArticle(this.userID,this.key_id).subscribe(
      //                                                                   article =>
      //                                                                   {
      //                                                                     console.log("this is the book.data title: ",article.data().doc)
      //                                                                     console.log("this is the book.data author: ",article.data().category)
      //                                                                     this.category = article.data().category;
      //                                                                     this.doc = article.data().doc;
      //                                                                   }
      //                                                                 );
      //   console.log(this.category)
      //   console.log(this.doc)
      // }
      // else
      {
        console.log("adding new article")
        this.isEdit=false;
      }
  }
  
  showArticle()
  {
     
        console.log("key_id not null")
        this.isEdit=true;
        console.log("this isEdit: ",this.isEdit)
        this.buttonText="Update article";
        console.log("Before this.articleService.getOneArticle(this.userID,this.key_id): userID: ", this.userID)
        this.articleService.getArticle(this.userID,this.key_id).subscribe(
                                                                        article =>
                                                                        {
                                                                          console.log("this is the article.data doc: ",article.data().doc)
                                                                          console.log("this is the article.data category: ",article.data().category)
                                                                          this.category = article.data().category;
                                                                          this.doc = article.data().doc;
                                                                        }
                                                                      );
        console.log(this.category)
        console.log(this.doc)
  }

  createArticle()
  {
    if(this.key_id==null || this.key_id==undefined)
    {
      this.articleService.addArticles(this.userID,this.doc, this.category);
      console.log("Article added");
      this.router.navigate(['/classtified_collection']);
    }
    else
    {
      this.key_id=this.route.snapshot.params.key_id;
      console.log("In the createArticles() in else")
      console.log("this.key_id= ",this.key_id)
      console.log("this.doc= ",this.doc)
      console.log("this.category= ",this.category)
      
      // this.articleService.updateArticle(this.userID,this.key_id,this.doc,this.category);
      this.articleService.updateArticle(this.userID,this.doc,this.key_id,this.category);

      this.router.navigate(['/classtified_collection']);
    }
  }




}
