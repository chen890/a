import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreCollectionGroup } from '@angular/fire/firestore';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService 
{
  userCollection: AngularFirestoreCollection= this.db.collection('Users');
  articleCollection: AngularFirestoreCollection;

  constructor(public db:AngularFirestore,
              public authService:AuthService,
              )
              { }

getArticles(userID): Observable<any[]> 
    {
      // return this.db.collection('Books').valueChanges({idField:'key_id'});
      this.articleCollection = this.db.collection(`Users/${userID}/Articles`);
      //console.log("this.articleCollection: ",this.articleCollection)
      return this.articleCollection.snapshotChanges().pipe(
                                                          map(
                                                            collection => collection.map(
                                                                                          document =>
                                                                                          {
                                                                                            const data= document.payload.doc.data();
                                                                                            data.id= document.payload.doc.id;
                                                                                            return data;
                                                                                          }
                                                                                        )
                                                            )
                                                        );
    } 

    deleteArticleFromCollection(userID, key_id)
    {
      console.log("In - books.ts deleteArticleFromCollection() ")
      console.log("This is the key_id: "+key_id)
      this.db.doc(`Users/${userID}/Articles/${key_id}`).delete();
      console.log(key_id+" have deleted")
    }

    updateArticle(userID:string,doc:string,key_id:string,category:string)
    {
      this.db.doc(`Users/${userID}/Articles/${key_id}`).update(
        {
          doc:doc,
          category:category,
        }
      )
    }
    getOneArticle(userID:string,key_id:string):Observable<any>
    {
      console.log("in getOneArticle key_id: ",key_id)
      console.log("in getOneArticle userID: ",userID)
      // console.log("search book: ",(this.db.doc(`Users/${userID}/Books/${id}`).get()));
      return this.db.doc(`Users/${userID}/Articles/${key_id}`).get();
      
    }

    getArticle(userID:string,key_id:string):Observable<any>
    {
      console.log("in getOneArticle key_id: ",key_id)
      console.log("in getOneArticle userID: ",userID)
      // console.log("search book: ",(this.db.doc(`Users/${userID}/Books/${id}`).get()));
      return this.db.doc(`Users/${userID}/Articles/${key_id}`).get();
      
    }
    addArticles(userID:string, doc:string, category:string)
    {
        console.log('userID in addBooks(): ', userID)
        const article={ userID:userID, doc:doc, category:category}
        //this.db.collection('Books').add(book);
        this.userCollection.doc(userID).collection('Articles').add(article);
        
    }


}
