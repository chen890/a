import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import { FormsModule } from '@angular/forms';
import { MatInputModule, MatSelectModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { HttpClientModule } from '@angular/common/http';
import { environment } from '../environments/environment';

import {MatExpansionModule} from '@angular/material/expansion';
import { AngularFirestoreModule } from '@angular/fire/firestore';
// import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule, AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from './auth.service';


import { LoginComponent } from './login/login.component';
import { NavComponent } from './nav/nav.component';
import { SignupComponent } from './signup/signup.component';
import { MainComponent } from './main/main.component';
import { ClasstifiedCollectionComponent } from './classtified-collection/classtified-collection.component';
import { SignupsuceessComponent } from './signupsuceess/signupsuceess.component';
import { ArticleFormComponent } from './article-form/article-form.component';
import { ClassifiedArticleComponent } from './classified-article/classified-article.component';
import { ArticleEditComponent } from './article-edit/article-edit.component';
import { TempformComponent } from './tempform/tempform.component';
import { TemperaturesComponent } from './temperatures/temperatures.component';



import { RouterModule, Routes } from '@angular/router';


const appRoutes: Routes=[
  { path: 'signup', component: SignupComponent },
  { path: 'article-form', component: ArticleFormComponent },
  { path: 'article_edit', component: ArticleEditComponent },
  { path: 'article_edit/:key_id', component: ArticleEditComponent },
  { path: 'tempertures', component: TemperaturesComponent },
  { path: 'temperatures/:city', component: TemperaturesComponent },
  { path: 'tempform', component: TempformComponent },

  { path: 'classified-article', component: ClassifiedArticleComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'signupsuceess', component: SignupsuceessComponent },
  { path: 'login', component: LoginComponent },
  { path: 'main', component: MainComponent },
  { path: 'classtified_collection', component: ClasstifiedCollectionComponent },
  // {path: 'login', component: LoginComponent},
  {path:"", redirectTo:'/login', pathMatch:'full'},
];









@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    SignupComponent,
    LoginComponent,
    MainComponent,
    ClasstifiedCollectionComponent,
    SignupsuceessComponent,
    ArticleFormComponent,
    ClassifiedArticleComponent,
    ArticleEditComponent,
    TemperaturesComponent,
    TempformComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule, 
    MatCardModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    FormsModule,
    HttpClientModule,
    AngularFireModule,
    AngularFirestoreModule,
    // AngularFireStorageModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebase),
    RouterModule
    .forRoot(
      appRoutes,
    //   {enableTracing:true}    // this is for debugging only
    )

  ],
  providers: [AngularFireAuth,AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
