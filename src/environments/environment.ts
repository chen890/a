// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyAZ5tAoV6n4A-fHVq8tZVRk0IaPv3eCf98",
    authDomain: "angular-test-92dbf.firebaseapp.com",
    databaseURL: "https://angular-test-92dbf.firebaseio.com",
    projectId: "angular-test-92dbf",
    storageBucket: "angular-test-92dbf.appspot.com",
    messagingSenderId: "296545687102",
    appId: "1:296545687102:web:e9cd28e70b11e40f70236f",
    measurementId: "G-MHJRJFK1PR"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
